class Post < ApplicationRecord
  validates_presence_of :title, minimum: 5
  validates_presence_of :body
  has_many :comments, dependent: :destroy
end
